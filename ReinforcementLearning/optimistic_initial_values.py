import numpy as np
import matplotlib.pyplot as plt


class Bandit:

	def __init__(self, m, upper_limit):
		# true mean
		self.m = m
		# optimistic value
		self.mean = upper_limit
		# number of samples
		self.N = 0

	def pull(self):
		# return reward from random Gaussian distribution centered at self.m
		return np.random.randn() + self.m

	def update(self, reward):
		# update number of samples
		self.N += 1
		# recalculate mean using previous mean
		self.mean = (1 - 1/self.N)*self.mean + (1/self.N)*reward

def conduct_trial(mean_1, upper_limit=10, mean_2, mean_3, eps, N):
	# initialize bandits
	bandits = [Bandit(mean_1, upper_limit), Bandit(mean_2, upper_limit), Bandit(mean_3, upper_limit)]
	# data for graph
	data = np.empty(N)
	# iterate through given number of trials
	for i in range(N):
		# greedy only strategy
		temp = np.argmax([sample.mean for sample in bandits])
		# get reward for chosen bandit
		reward = bandits[temp].pull()
		# update mean for given action
		bandits[temp].update(reward)
		# add reward to data for graph
		data[i] = reward





