import numpy as np
import matplotlib.pyplot as plt


class Bandit:

	def __init__(self, m):
		# true mean
		self.m = m
		# estimated mean
		self.mean = 0
		# number of samples
		self.N = 0

	def pull(self):
		# return reward from random Gaussian distribution centered at self.m
		return np.random.randn() + self.m

	def update(self, reward):
		# update number of samples
		self.N += 1
		# recalculate mean using previous mean
		self.mean = (1 - 1/self.N)*self.mean + (1/self.N)*reward

def conduct_trial(mean_1, mean_2, mean_3, eps, N):
	bandits = [Bandit(mean_1), Bandit(mean_2), Bandit(mean_3)]
	# data for graph
	data = np.empty(N)
	# iterate through given number of trials
	for i in range(N):
		# get random value
		p = np.random.random()
		# epislon greedy
		if p < eps:
			# if less than epsilon, explore
			temp = np.random.choice(3)
		else:
			# if greater than epsilon, exploit
			temp = np.argmax([sample.mean for sample in bandits])
		# get reward for chosen bandit
		reward = bandits[temp].pull()
		# update mean for given action
		bandits[temp].update(reward)
		# add reward to data for graph
		data[i] = reward





