import numpy as np
import matplotlib.pyplot as plt


class Agent:
	def __init__(self, eps=0.1, alpha=0.5):
		self.eps = eps
		self.alpha = alpha
		self.verbose = False
		self.state_history = []
	
	def set_value_table(self, value):
		self.value_table = value

	def set_symbol(self, symbol):
		self.symbol = symbol

	def set_verbose(self, v):
		self.verbose = v

	def reset_history(self):
		self.state_history = []

	def take_action(self, environment):
		r = np.random.rand()
		best_state = None
		if r < self.eps:
			if self.verbose:
				print('Taking a random action')
			possible_moves = []
			for i in range(3):
				for j in range(3):
					if environment.is_empty(i,j):
						possible_moves.append((i,j))
			idx = np.random.choice(len(possible_moves))
			next_move = possible_moves[idx]
		else:
			pos2value = {}
			next_move = None
			best_value = -1
			for i in range(3):
				for j in range(3):
					if environment.is_empty(i,j):
						environment.board[i,j] = self.symbol
						state = environment.get_state()
						environment.board[i,j] = 0
						pos2value[(i,j)] = self.value_table[state]
						if self.value_table[state] > best_value:
							best_value = self.value_table[state]
							best_state = state
							next_move = (i,j)
			if self.verbose:
				print('Taking a greedy action')
				for i in range(3):
					print('------------------')
					for j in range(3):
						if environment.is_empty(i,j):
							# print the value
							print('{:.2f}|'.format(pos2value[(i,j)]), end='')
						else:
							print('  ', end='')
							if environment.board[i,j] == environment.x:
								print('x  |', end='')
							elif environment.board[i,j] == environment.o:
								print('o  |', end='')
							else:
								print('   |', end='')
					print('')
				print('------------------')
		environment.board[next_move[0], next_move[1]] = self.symbol

	def update_state_history(self, s):
		self.state_history.append(s)

	def update(self, environment):
		reward = environment.reward(self.symbol)
		target = reward
		for previous in reversed(self.state_history):
			value = self.value_table[previous] + self.alpha*(target - self.value_table[previous])
			self.value_table[previous] = value
			target = value
		self.reset_history()


class Environment:
	def __init__(self):
		self.board = np.zeros(shape=(3, 3))
		self.x = -1
		self.o = 1
		self.winner = None
		self.ended = False
		self.number_of_states = 3**(3*3)

	def is_empty(self, i,j):
		return self.board[i,j] == 0

	def reward(self, symbol):
		if not self.game_over():
			return 0
		return 1 if self.winner == symbol else 0

	def get_state(self):
		h = []
		for i in range(3):
			for j in range(3):
				if self.board[i,j] == 0:
					v = 0
				elif self.board[i,j] == self.x:
					v = 1
				elif self.board[i,j] == self.o:
					v = 2
				h.append(v)
		h = reversed(h)
		h = ''.join(map(str, h))
		# print(int(h, 3))
		return int(h, 3)

	def game_over(self, force_recalculate=False):
		if not force_recalculate and self.ended:
			return self.ended
		
		for i in range(3):
			for player in (self.x, self.o):
				if self.board[i].sum() == player*3:
					self.winner = player
					self.ended = True
					return True

		for j in range(3):
			for player in (self.x, self.o):
				if self.board[:,j].sum() == player*3:
					self.winner = player
					self.ended = True
					return True

		for player in (self.x, self.o):
			if self.board.trace() == player*3:
				self.winner = player
				self.ended = True
				return True
			if np.fliplr(self.board).trace() == player*3:
				self.winner = player
				self.ended = True
				return True

		if np.all((self.board == 0) == False):
			# winner stays None
			self.winner = None
			self.ended = True
			return True

		self.winner = None
		return False

	def is_draw(self):
		return self.ended and self.winner is None

	def draw_board(self):
		for i in range(3):
			print('-------------')
			for j in range(3):
				print('  ', end='')
				if self.board[i,j] == self.x:
					print('x ', end='')
				elif self.board[i,j] == self.o:
					print('o ', end='')
				else:
					print('  ', end='')
			print('')
		print('-------------')



class Human:
	def __init__(self):
		pass

	def set_symbol(self, symbol):
		self.symbol = symbol

	def take_action(self, environment):
		while True:
			move = input('Enter coordinates i,j for your next move:')
			i,j = move.split(',')
			i = int(i)
			j = int(j)
			if environment.is_empty(i,j):
				environment.board[i,j] = self.symbol
				break

	def update(self, environment):
		pass

	def update_state_history(self, s):
		pass


def get_state_hash_and_winner(environment, i=0, j=0):
	results = []

	for v in (0, environment.x, environment.o):
		environment.board[i,j] = v
		if j == 2:
			if i == 2:
				state = environment.get_state()
				ended = environment.game_over(force_recalculate=True)
				winner = environment.winner
				results.append((state, winner, ended))
			else:
				results += get_state_hash_and_winner(environment, i + 1, 0)
		else:
			results += get_state_hash_and_winner(environment, i,j + 1)
	return results

def initialize_value_x(environment, state_winner_tuples):
	value_table = np.zeros(environment.number_of_states)
	for state, winner, ended in state_winner_tuples:
		if ended:
			if winner == environment.x:
				v = 1
			else:
				v = 0
		else:
			v = 0.5
		value_table[state] = v
	return value_table

def initialize_value_o(environment, state_winner_tuples):
	value_table = np.zeros(environment.number_of_states)
	for state, winner, ended in state_winner_tuples:
		if ended:
			if winner == environment.o:
				initial_value = 1
			else:
				initial_value = 0
		else:
			initial_value = 0.5
		value_table[state] = initial_value
	return value_table


def play_game(player_1, player_2, environment, draw=False):
	current_player = None
	while not environment.game_over():
		if current_player == player_1:
			current_player = player_2
		else:
			current_player = player_1
		if draw:
			if draw == 1 and current_player == player_1:
				environment.draw_board()
			if draw == 2 and current_player == player_2:
				environment.draw_board()
		current_player.take_action(environment)
		state = environment.get_state()
		player_1.update_state_history(state)
		player_2.update_state_history(state)
	if draw:
		environment.draw_board()
	player_1.update(environment)
	player_2.update(environment)


if __name__ == '__main__':
	player_1 = Agent()
	player_2 = Agent()

	environment = Environment()
	state_winner_tuples = get_state_hash_and_winner(environment)


	x_value_table = initialize_value_x(environment, state_winner_tuples)
	player_1.set_value_table(x_value_table)
	o_value_table = initialize_value_o(environment, state_winner_tuples)
	player_2.set_value_table(o_value_table)

	player_1.set_symbol(environment.x)
	player_2.set_symbol(environment.o)

	epochs = 20000
	for episode in range(epochs):
		if episode % 200 == 0:
			print('Training episode #{}'.format(episode))
		play_game(player_1, player_2, Environment())


	human = Human()
	human.set_symbol(environment.o)
	while True:
		player_1.set_verbose(True)
		play_game(player_1, human, Environment(), draw=2)
		answer = input('Play again? [Y/n]: ')
		if answer and answer.lower()[0] == 'n':
			break