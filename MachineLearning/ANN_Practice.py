import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix, accuracy_score
import keras
from keras.models import Sequential
from keras.layers import Dense


dataset = pd.read_csv('/Users/Marko/Downloads/Artificial_Neural_Networks/Churn_Modelling.csv')
X = dataset.iloc[:,3:13]
y = dataset['Exited'].values

geography = pd.get_dummies(X['Geography'], drop_first=True)
gender = pd.get_dummies(X['Gender'], drop_first=True)

X = pd.concat([geography, gender, X], axis=1)
X.drop(['Geography', 'Gender'], inplace=True, axis=1)
X = X.values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=101)

scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

training_epochs = 100
n_input = 11
n_hidden_1 = 6
n_hidden_2 = 6
n_output = 1

classifier = Sequential()
classifier.add(Dense(output_dim=n_hidden_1, init='normal', activation='relu', input_dim=n_input))
classifier.add(Dense(output_dim=n_hidden_2, init='normal', activation='relu'))
classifier.add(Dense(output_dim=n_output, init='normal', activation='sigmoid'))
classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

classifier.fit(X_train, y_train, batch_size=10, nb_epoch=training_epochs)

y_pred = classifier.predict(X_test)
y_pred = (y_pred > 0.5)
print(confusion_matrix(y_test, y_pred))
print('Accuracy:', accuracy_score(y_test, y_pred))
