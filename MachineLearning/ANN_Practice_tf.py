import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix, accuracy_score
import tensorflow as tf


dataset = pd.read_csv('/Users/Marko/Downloads/Artificial_Neural_Networks/Churn_Modelling.csv')
X_data = dataset.iloc[:, 3:13]
y_data = dataset['Exited'].values

geography = pd.get_dummies(X_data['Geography'], drop_first=True)
gender = pd.get_dummies(X_data['Gender'], drop_first=True)

X_data = pd.concat([geography, gender, X_data], axis=1)
X_data.drop(['Geography', 'Gender'], inplace=True, axis=1)
X_data = X_data.values

X_train, X_test, y_train, y_test = train_test_split(X_data, y_data, test_size=0.3)

scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

y_train = y_train.reshape((-1,1))

training_epochs = 400
n_input = 11
n_hidden_1 = 10
n_hidden_2 = 10
n_hidden_3 = 10
n_hidden_4 = 10
n_output = 1

def neuralNetwork(x, weights, biases, keep_prob):
	layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
	layer_1 = tf.nn.relu(layer_1)
	layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
	layer_2 = tf.nn.relu(layer_2)
	layer_3 = tf.add(tf.matmul(layer_2, weights['h3']), biases['b3'])
	layer_3 = tf.nn.relu(layer_3)
	layer_4 = tf.add(tf.matmul(layer_3, weights['h4']), biases['b4'])
	layer_4 = tf.nn.relu(layer_4)
	logits = tf.add(tf.matmul(layer_4, weights['output']), biases['output'])
	drop_out = tf.nn.dropout(x=logits, keep_prob=keep_prob)
	predictions = tf.nn.sigmoid(drop_out)
	return logits, predictions

weights = {
	'h1': tf.Variable(tf.random_normal([n_input, n_hidden_1])),
	'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
	'h3': tf.Variable(tf.random_normal([n_hidden_2, n_hidden_3])),
	'h4': tf.Variable(tf.random_normal([n_hidden_3, n_hidden_4])),
	'output': tf.Variable(tf.random_normal([n_hidden_4, n_output]))
}

biases = {
	'b1': tf.Variable(tf.zeros([n_hidden_1])),
	'b2': tf.Variable(tf.zeros([n_hidden_2])),
	'b3': tf.Variable(tf.zeros([n_hidden_3])),
	'b4': tf.Variable(tf.zeros([n_hidden_3])),
	'output': tf.Variable(tf.zeros([n_output]))
}

x = tf.placeholder('float', [None, n_input]) # [?, 11]
y = tf.placeholder('float', [None, n_output]) # [?, 1]
keep_prob = tf.placeholder(tf.float32)

logits, output = neuralNetwork(x, weights, biases, keep_prob)

# cost = tf.reduce_mean(tf.square(output - y))
cost = tf.nn.sigmoid_cross_entropy_with_logits(labels=y, logits=logits)
optimizer = tf.train.AdamOptimizer().minimize(cost)

with tf.Session() as session:
	session.run(tf.global_variables_initializer())

	for epoch in range(training_epochs + 1):
		print('epoch #{}'.format(epoch))
		session.run(optimizer, feed_dict={x: X_train, y: y_train, keep_prob: 0.3})

	print('Model has completed training.')
	test = session.run(output, feed_dict={x:X_test, keep_prob:1.0})
	predicted_labels = (test>0.5).astype(int)
	# print(predicted_labels[0:10])
	print('confusion matrix:', confusion_matrix(y_test, predicted_labels), sep='\n')
	print('accuracy:', accuracy_score(y_test, predicted_labels))








