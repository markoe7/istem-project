import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


columnNames = ['user_id', 'item_id', 'rating', 'timestamp']

df = pd.read_csv('/Users/Marko/Downloads/ML Course/Machine Learning Sections/Recommender-Systems/u.data', names=columnNames, sep='\t')
# print(df.head())

movieTitles = pd.read_csv('/Users/Marko/Downloads/ML Course/Machine Learning Sections/Recommender-Systems/Movie_Id_Titles')
# print(movieTitles.head())

df = pd.merge(df, movieTitles, on='item_id')
# print(df.head())

ratings = pd.DataFrame(df.groupby('title')['rating'].mean())
ratings['num of ratings'] = pd.DataFrame(df.groupby('title')['rating'].count())
# print(ratings.head())

movieMatrix = df.pivot_table(index='user_id', columns='title', values='rating')

ratings.sort_values('num of ratings', ascending=False)
# print(ratings.head())

starwarsUserRatings = movieMatrix['Star Wars (1977)']
liarliarUserRatings = movieMatrix['Liar Liar (1997)']

similarToStarWars = movieMatrix.corrwith(starwarsUserRatings)
similarToLiarLiar = movieMatrix.corrwith(liarliarUserRatings)

corrStarWars = pd.DataFrame(data=similarToStarWars, columns=['Correlation'])
corrStarWars.dropna(inplace=True)
# print(corrStarWars.head())

corrStarWars = corrStarWars.join(ratings['num of ratings'])
# print(corrStarWars.head())

corrStarWars = corrStarWars[corrStarWars['num of ratings'] > 100].sort_values('Correlation', ascending=False)
print(corrStarWars.head())

