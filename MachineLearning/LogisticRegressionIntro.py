import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.linear_model import LogisticRegression

trainingData = pd.read_csv('/Users/Marko/Downloads/Python-Data-Science-and-Machine-Learning-Bootcamp/Machine Learning Sections/Logistic-Regression/titanic_train.csv')
# print(trainingData.head())

def imputeAge(cols):
	age = cols[0]
	pClass = cols[1]
	if pd.isnull(age):
		if pClass == 1:
			return 37
		elif pClass == 2:
			return 29
		else:
			return 24
	else:
		return age

trainingData['Age'] = trainingData[['Age', 'Pclass']].apply(imputeAge, axis=1)
trainingData.drop('Cabin', axis=1, inplace=True)

# sns.heatmap(trainingData.isnull(), yticklabels=False, cbar=False, cmap='coolwarm')
# plt.show()

sex = pd.get_dummies(trainingData['Sex'], drop_first=True)
embarked = pd.get_dummies(trainingData['Embarked'], drop_first=True)
trainingData = pd.concat([trainingData, sex, embarked], axis=1)
trainingData.drop(['Sex', 'Embarked', 'Name', 'Ticket', 'PassengerId'], axis=1, inplace=True)
print(trainingData.head())

X = trainingData.drop('Survived', axis=1)
y = trainingData['Survived']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=101)

logModel = LogisticRegression()
logModel.fit(X_train, y_train)

predictions = logModel.predict(X_test)
print(classification_report(y_test, predictions))
