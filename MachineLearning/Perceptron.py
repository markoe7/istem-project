import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import  matplotlib.pyplot as plt

mnist = input_data.read_data_sets('/tmp/data', one_hot=True)

# sample = mnist.train.images[234].reshape(28,28)
# plt.imshow(sample, cmap='Greys')
# plt.show()

learning_rate = 0.001
training_epochs = 15
batch_size = 100

n_classes = 10
n_samples = mnist.train.num_examples
n_input = 784

n_hidden_1 = 256
n_hidden_2 = 256

def multiLayerPerceptron(x, weights, biases):
	'''
	x: placeholder for data input
	weights: dict. of synaptic weights
	biases: dict. of bias values
	'''
	# first hidden layer with RELU activation
	# X * weights + bias
	# note: relu outputs 0 if input is negative and the input if positive
	layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
	layer_1 = tf.nn.relu(layer_1)
	# second hidden layer
	layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
	layer_2 = tf.nn.relu(layer_2)
	# output layer
	out_layer = tf.add(tf.matmul(layer_2, weights['out']), biases['out'])
	return out_layer

weights = {
	'h1': tf.Variable(tf.random_normal([n_input, n_hidden_1])),
	'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
	'out': tf.Variable(tf.random_normal([n_hidden_2, n_classes]))
}

biases = {
	'b1': tf.Variable(tf.random_normal([n_hidden_1])),
	'b2': tf.Variable(tf.random_normal([n_hidden_2])),
	'out': tf.Variable(tf.random_normal([n_classes]))
}

x = tf.placeholder('float', [None, n_input])
y = tf.placeholder('float', [None, n_classes])

pred = multiLayerPerceptron(x, weights, biases)

cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

sess = tf.InteractiveSession()
init = tf.initialize_all_variables()
sess.run(init)
for epoch in range training_epochs:
	avg_cost = 0.0
	total_batch = int(n_samples/batch_size)
	for i in range(total_batch):
		batch_x, batch_y = mnist.train.next_batch(batch_size)
		_,c = sess.run([optimizer, cost], feed_dict={x:batch_x, y:batch_y})
		avg_cost = c/total_batch
	print('Epoch: {}, Cost: {:.4f}'.format(epoch+1, avg_cost))
print('Model has completed training.')





