import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.ensemble import RandomForestClassifier

df = pd.read_csv('/Users/Marko/Downloads/ML Course/Machine Learning Sections/Decision-Trees-and-Random-Forests/kyphosis.csv')
print(df.head())

X = df.drop('Kyphosis', axis=1)
y = df['Kyphosis']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

dtree = DecisionTreeClassifier()
dtree.fit(X_train, y_train)

predictions = dtree.predict(X_test)

print(confusion_matrix(y_test, predictions))
print(classification_report(y_test, predictions))

rfc = RandomForestClassifier(n_estimators=200)
rfc.fit(X_train, y_train)

rfcPredictions = rfc.predict(X_test)

print(confusion_matrix(y_test, rfcPredictions))
print(classification_report(y_test, rfcPredictions))
