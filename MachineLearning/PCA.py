import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.datasets import load_breast_cancer

cancer = load_breast_cancer()

df = pd.DataFrame(data=cancer['data'], columns=cancer['feature_names'])
print(df.head())

scaler = StandardScaler()
scaler.fit(df)
scaledData = scaler.transform(df)

pca = PCA(n_components=2)
pca.fit(scaledData)
reducedData = pca.transform(scaledData)

plt.scatter(reducedData[:,0],reducedData[:,1], c=cancer['target'], cmap='plasma')
plt.show()