import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import load_breast_cancer
from sklearn.svm import SVC
from sklearn.grid_search import GridSearchCV


cancer = load_breast_cancer()

features = pd.DataFrame(data=cancer['data'], columns=cancer['feature_names'])
print(features.head(3))

X = features
y = cancer['target']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=101)

model = SVC()

model.fit(X_train, y_train)

predictions = model.predict(X_test)

print(confusion_matrix(y_test, predictions))
print(classification_report(y_test, predictions))

paramGrid = {'C':[0.1,1,10,100,1000], 'gamma':[1,0.1,0.01,0.001,0.0001]}
grid = GridSearchCV(SVC(), paramGrid, verbose=3)
grid.fit(X_train, y_train)

gridPredictions = grid.predict(X_test)

print(confusion_matrix(y_test, gridPredictions))
print(classification_report(y_test, gridPredictions ))