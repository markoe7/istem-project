import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix


df = pd.read_csv('/Users/Marko/Downloads/ML Course/Machine Learning Sections/K-Nearest-Neighbors/Classified Data.csv', index_col=0)

# print(df.head())

scalar = StandardScaler()
scalar.fit(df.drop('TARGET CLASS', axis=1))

scaledFeatures = scalar.transform(df.drop('TARGET CLASS', axis=1))

featuresDF = pd.DataFrame(scaledFeatures, columns=df.columns[:-1])

X = featuresDF
y = df['TARGET CLASS']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=101)

knn = KNeighborsClassifier(n_neighbors=17)
knn.fit(X_train, y_train)

predictions = knn.predict(X_test)

print(confusion_matrix(y_test, predictions))
print(classification_report(y_test, predictions))

errorRate = []

for i in range(1, 40):
	knn = KNeighborsClassifier(n_neighbors=i)
	knn.fit(X_train, y_train)
	predictionI = knn.predict(X_test)
	# takes mean of predicted values that are not equal to actual values
	# serves as our error
	errorRate.append(np.mean(predictionI != y_test))

plt.plot(range(1,40), errorRate, color='blue', linestyle='dashed', marker='o', markersize=10, markerfacecolor='red')
plt.xlabel = 'K Value'
plt.ylabel = 'Error'
plt.show()

