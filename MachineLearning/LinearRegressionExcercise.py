import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics

df = pd.read_csv('/Users/Marko/Downloads/Python-Data-Science-and-Machine-Learning-Bootcamp/Machine Learning Sections/Linear-Regression/Ecom.csv')
print(df.head())

# sns.lmplot(data=df, x='Length of Membership', y='Yearly Amount Spent', palette='viridis')
# plt.show()

# print(df.columns)

y = df['Yearly Amount Spent']
X = df[['Avg. Session Length','Time on App','Time on Website', 'Length of Membership']]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=101)

lm = LinearRegression()
lm.fit(X_train, y_train)

predictions = lm.predict(X_test)

# plt.scatter(y_test, predictions)
# plt.xlabel = 'Y Test'
# plt.ylabel = 'Predictions'
# plt.show()

# sns.jointplot(x=y_test, y=predictions, kind='hex')
# plt.show()