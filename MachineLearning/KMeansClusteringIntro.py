import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler


# data = make_blobs(n_samples=200, n_features=2, centers=4, cluster_std=1.8, random_state=101)
# # print(data[1])

# # x=data[0][:,0] --> all first ([:,0]) rows from feature column (column 0)
# # y=data[0][:,1] --> all second ([:,1]) rows from feature column (column 0)


# # plt.scatter(x=data[0][:,0], y=data[0][:,1], c=data[1], cmap='rainbow')
# # plt.show()

# kmeans = KMeans(n_clusters=4)
# kmeans.fit(data[0])

# # plt.scatter(x=data[0][:,0], y=data[0][:,1], c=kmeans.labels_, cmap='rainbow')
# # plt.show()

df = pd.read_csv('/Users/Marko/Downloads/ML Course/Machine Learning Sections/K-Means-Clustering/College_Data')
# print(df.head())

def convert(data):
	if data == 'Yes':
		return 1
	else:
		return 0

df['Target'] = df['Private'].apply(convert)
# print(df.columns[2:-1])

scaler = MinMaxScaler()
scaler.fit(df.drop(['Private', 'Unnamed: 0', 'Target'], axis=1))
features = scaler.transform(df.drop(['Private', 'Unnamed: 0', 'Target'], axis=1))
featureDF = pd.DataFrame(data=features, columns=df.columns[2:-1])

kmeans = KMeans(n_clusters=2)
kmeans.fit(featureDF)

print(classification_report(df['Target'], kmeans.labels_))
print(confusion_matrix(df['Target'], kmeans.labels_))

