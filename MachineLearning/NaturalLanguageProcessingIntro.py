import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import nltk
import string
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.ensemble import RandomForestClassifier


# nltk.download_shell()

# messages = [line.rstrip() for line in open('/Users/Marko/Downloads/ML Course/Machine Learning Sections/Natural-Language-Processing/smsspamcollection/SMSSpamCollection')]
# print(messages[0])

messages = pd.read_csv('/Users/Marko/Downloads/ML Course/Machine Learning Sections/Natural-Language-Processing/smsspamcollection/SMSSpamCollection', sep='\t', names=['label', 'message'])

messages['length'] = messages['message'].apply(len)

def processMessage(message):
	noPunctuation = [char for char in message if char not in string.punctuation]
	noPunctuation = ''.join(noPunctuation)
	return [word for word in noPunctuation.split() if word.lower() not in stopwords.words('english')]

messages['message'] = messages['message'].apply(processMessage)
# print(messages.head())

transformer = CountVectorizer(analyzer=processMessage).fit(messages['message'])
print(transformer.vocabulary_)

messagesBagOfWords = transformer.transform(messages['message'])

tfidfTransformer = TfidfTransformer().fit(messagesBagOfWords)

tfidf = tfidfTransformer.transform(messagesBagOfWords)

spamDetectModel = MultinomialNB().fit(tfidf, messages['label'])

msgTrain, msgTest, labelTrain, labelTest = train_test_split(messages['message'], messages['label'], test_size=0.3)

pipeline = Pipeline([
	('bow',CountVectorizer(analyzer=processMessage)), 
	('tfidf',TfidfTransformer()),
	('classifier',MultinomialNB())])

pipeline.fit(msgTrain, labelTrain)

predictions = pipeline.predict(msgTest)

# print(confusion_matrix(labelTest, predictions))
# print(classification_report(labelTest, predictions))



