import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix, accuracy_score
import tensorflow as tf
import random


actions = 9

won_games = 0
lost_games = 0
drawn_games = 0

OPPONENT = -1
AI = 1

n_input = actions
n_output = actions

n_hidden_1 = 750
n_hidden_2 = 750
n_hidden_3 = 750

epsilon = 0.9

discount_factor = 0.9

def get_empty_spots(currentBoard):
	# return available spots to move
	emptyCells = []
	for x, row in enumerate(currentBoard):
		for y, cell in enumerate(row):
			if cell == 0:
				emptyCells.append([x, y])
	return emptyCells

def check_winning_states(currentBoard, player):
	# winning combinations
	winState = [
		[currentBoard[0][0], currentBoard[0][1], currentBoard[0][2]],
		[currentBoard[1][0], currentBoard[1][1], currentBoard[1][2]],
		[currentBoard[2][0], currentBoard[2][1], currentBoard[2][2]],
		[currentBoard[0][0], currentBoard[1][0], currentBoard[2][0]],
		[currentBoard[0][1], currentBoard[1][1], currentBoard[2][1]],
		[currentBoard[0][2], currentBoard[1][2], currentBoard[2][2]],
		[currentBoard[0][0], currentBoard[1][1], currentBoard[2][2]],
		[currentBoard[2][0], currentBoard[1][1], currentBoard[0][2]],]
	# check if player has 3 in a row in currentBoard
	if [player, player, player] in winState:
		return True
	else:
		return False

def did_ai_win(currentBoard):
	# returns True if someone won
	return checkGameOver(currentBoard, AI)

def did_opponent_win(currentBoard):
	# returns True if someone won
	return checkGameOver(currentBoard, OPPONENT)

def invert_board(board):
	temp = np.copy(board)
	for x, row in enumerate(board):
		for y, cell in enumerate(row):
			cell *= -1
	return board.reshape(-1)


weights = {
	'h1': tf.Variable(tf.truncated_normal([n_input, n_hidden_1])),
	'h2': tf.Variable(tf.truncated_normal([n_hidden_1, n_hidden_2])),
	'h3': tf.Variable(tf.truncated_normal([n_hidden_2, n_hidden_3])),
	'output': tf.Variable(tf.truncated_normal([n_hidden_3, n_output]))
}

biases = {
	'b1': tf.Variable(tf.zeros([n_hidden_1])),
	'b2': tf.Variable(tf.zeros([n_hidden_2])),
	'b3': tf.Variable(tf.zeros([n_hidden_3])),
	'output': tf.Variable(tf.zeros([n_output]))
}

def create_network(x, weights, biases):
	layer_1 = tf.nn.relu(tf.add(tf.matmul(x, weights['h1']), biases['b1']))
	layer_2 = tf.nn.relu(tf.add(tf.matmul(layer_1, weights['h2']), biases['b2']))
	layer_3 = tf.nn.relu(tf.add(tf.matmul(layer_2, weights['h3']), biases['b3']))
	Q_output = tf.add(tf.matmul(layer_3, weights['output']), biases['output'])
	prediction = tf.argmax(Q_output[0]) # largest index in row
	return x, Q_output, prediction

def train_network():
	X = tf.placeholder('float', [None, actions])
	targetQOutputs = tf.placeholder('float', [None, actions])

	inputState, Qoutputs, prediction = create_network(X, weights, biases)
	loss = tf.reduce_mean(tf.square(tf.subtract(targetQOutputs, Qoutputs)))
	optimizer = tf.train.AdamOptimizer().minimize(loss)

	sess = tf.InteractiveSession()
	sess.run(tf.global_variables_initializer())
	
	total_matches = 6000
	matches_per_episode = 500
	total_iterations = total_matches / matches_per_episode
	number_of_episodes = 500

	iterations = 0
	step = 0

	while True:
		global won_games
		global lost_games
		global drawn_games

		epsilon_decay_rate = 0.9/total_iterations
		total_loss = 0
		epochs = 100
		games_list = []
		epsilon = 0.9

		for i in range(number_of_episodes):
			complete_game_memory, victory = play_a_game(epsilon, sess, inputState, prediction, Qoutputs)
			games_list.append(complete_game_memory)

		for j in range(epochs):
			random.shuffle(games_list)
			for game in games_list:
				len_game = len(game)
				loop_in = 0
				game_reward = 0
				while loop_in < len_game:
					current_memory = game.pop()
					current_state = current_memory[0]
					action = current_memory[1][0]
					reward = current_memory[2][0]
					next_state = current_memory[3]
					if loop_in == 0:
						game_reward = reward
					else:
						# get q values for next state
						next_q = sess.run([Qoutputs], feed_dict={inputState: [next_state]})
						max_next_q = np.max(next_q)
						game_reward = discount_factor * max_next_q
					target_q = sess.run([Qoutputs], feed_dict={inputState: [current_state]})
					target_q[0][action] = game_reward
					loop_in += 1
					t_loss = 0
					t_loss = sess.run([optimizer, Qoutputs, loss], feed_dict={inputState: inputState, targetQOutputs: target_q})
					total_loss += t_loss[2]
		iterations += 1
		print('iteration #{} completed with {} wins, {} losses {} draws, out of {} games played, e is {} \ncost is {}'.format(iterations,
		won_games, lost_games, drawn_games, matches_per_episode, epsilon*100, total_loss))

		total_loss = 0
		won_games = 0
		lost_games = 0
		draw_games = 0

		if epsilon > -0.2:
			epsilon -= epsilon_decay_rate
		else:
			epsilon = random.choice([0.1,0.05,0.06,0.07,0.15,0.03,0.20,0.25,0.5,0.4])

def play_a_game(epsilon, session, inputState, prediction, Qoutputs):
	global won_games
	global lost_games
	global drawn_games

	win_reward = 10
	loss_reward = -10
	draw_reward = 5

	game_memory = []
	game_board = np.zeros((3, 3)).astype(int)

	turn = random.choice([1, -1])
	if turn == -1:
		opponent_index = random.choice(range(9))
		best_index, _ = session.run([prediction, Qoutputs], feed_dict={inputState: [np.copy(game_board).reshape(-1)]})
		opponent_index = random.choice([best_index, best_index, opponent_index])
		game_board[int(opponent_index/3), opponent_index%3] = -1
		turn = turn * -1

	while True:
		memory = [] # initial state, action taken, reward received, next state
		linear_board = np.array(np.copy(game_board).reshape(-1))
		available_indices = []
		for index, item in enumerate(linear_board):
			if item == 0:
				available_indices.append(index)
		if len(available_indices) == 0:
			reward = draw_reward
			drawn_games += 1
			game_memory[-1][2][0] = reward # last game reward = draw_reward
			break
		prediction,_ = session.run([prediction, Qoutputs], feed_dict={inputState: [linear_board]})
		is_prediction_valid = True if linear_board[prediction] == 0 else False
		memory.append(np.copy(game_board).reshape(-1))
		if random.random() > epsilon and is_prediction_valid:
			action = prediction
		else:
			random_index = random.choice(available_indices)
			action = random_index
		# append action to memory object
		memory.append(action)
		# update game board with action
		game_board[int(action/3)][action%3] = 1
		# calculate resulting reward
		reward = 0
		# action resulted in final state and AI won
		if did_ai_win(game_board):
			reward = win_reward
			memory.append([reward])
			memory.append(np.copy(game_board.reshape(-1)))
			game_memory.append(memory)
			won_games += 1
			break
		# opponent's turn
		temp_inverse = np.array(np.copy(invert_board(game_board)).reshape(-1))
		temp_copy = np.array(np.copy(game_board.reshape(-1)))
		available_indices = []
		for index, item in enumerate(linear_board):
			if item == 0:
				available_indices.append(index)
		if len(available_indices) == 0:
			reward = draw_reward
			drawn_games += 1
			game_memory[-1][2][0] = reward # last game reward = draw_reward
			break
		prediction,_ = session.run([prediction, Qoutputs], feed_dict={inputState: [temp_inverse]})
		is_prediction_valid = True if linear_board[prediction] == 0 else False
		action = None
		if is_prediction_valid:
			random_index = random.choice(available_indices)
			action = random.choice([random_index, prediction, prediction, prediction, prediction])
		else:
			action = random_index
		# update game_board with opponent's move
		game_board[int(action/3)][action%3] == -1
		# action resulted in final state and OPPONENT won
		if did_opponent_win(game_board):
			reward = loss_reward
			memory.append([reward])
			memory.append(np.copy(game_board.reshape(-1)))
			game_memory.append(memory)
			lost_games += 1
			break
		# if no winner, continue game
		memory.append([0])
		memory.append(np.copy(game_board).reshape(-1))
		game_memory.append(memory)
		return game_memory, reward


if __name__ == '__main__':
	train_network()























