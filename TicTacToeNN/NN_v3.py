import tensorflow as tf
import random
import numpy as np


# board dimensions (3x3)
boardSize = 9
# number of input/output neurons
n_input = n_output = 9

# game win/loss/draw stat trackers
won_games = 0
lost_games = 0
drawn_games = 0

# number of neurons in hidden layers
n_hidden_1 = 750
n_hidden_2 = 750
n_hidden_3 = 750

# game piece constants
OPPONENT = -1
AI = 1

# number of games to train with per training episode
episodes = 500
# of iterations from the total number of episodes to train on
epochs = 100
# chance that selected action will be random
epsilon = 1.0
# rate at which epsilon decreases every iteration
epsilon_decay_rate = 0.0075
# discount factor
# Q(s, a) = R(s, a) + Gamma * Max[Q(next s, a)]
GAMMA = 0.9


def invert_board(board):
	# get a copy of the current board state
	temp = np.copy(board)
	# iterate through board cells and get inverted value
	for x, row in enumerate(board):
		for y, cell in enumerate(row):
			cell *= -1
	# return properly shaped board
	return board.reshape(-1)

def check_winning_states(currentBoard, player):
	# winning combinations
	winState = [
		[currentBoard[0][0], currentBoard[0][1], currentBoard[0][2]],
		[currentBoard[1][0], currentBoard[1][1], currentBoard[1][2]],
		[currentBoard[2][0], currentBoard[2][1], currentBoard[2][2]],
		[currentBoard[0][0], currentBoard[1][0], currentBoard[2][0]],
		[currentBoard[0][1], currentBoard[1][1], currentBoard[2][1]],
		[currentBoard[0][2], currentBoard[1][2], currentBoard[2][2]],
		[currentBoard[0][0], currentBoard[1][1], currentBoard[2][2]],
		[currentBoard[2][0], currentBoard[1][1], currentBoard[0][2]],]
	# check if player has 3 in a row in currentBoard
	if [player, player, player] in winState:
		return True
	else:
		return False

def did_ai_win(currentBoard):
	# returns True if AI won
	return check_winning_states(currentBoard, AI)

def did_opponent_win(currentBoard):
	# returns True if OPPONENT won
	return check_winning_states(currentBoard, OPPONENT)

def create_network():
	# first hidden layer weights and biases
	weights_1 = tf.Variable(tf.truncated_normal([boardSize, n_hidden_1], stddev=0.01))
	biases_1 = tf.Variable(tf.constant(0.01, shape=[n_hidden_1])),
	# second hidden layer weights and biases
	weights_2 = tf.Variable(tf.truncated_normal([n_hidden_1, n_hidden_2], stddev=0.01))
	biases_2 = tf.Variable(tf.constant(0.01, shape=[n_hidden_2]))
	# third hidden layer weights and biases
	weights_3 = tf.Variable(tf.truncated_normal([n_hidden_2, n_hidden_3], stddev=0.01))
	biases_3 = tf.Variable(tf.constant(0.01, shape=[n_hidden_3]))
	# output layer weights and biases
	weights_output = tf.Variable(tf.truncated_normal([n_hidden_3, n_output], stddev=0.01))
	biases_output  = tf.Variable(tf.constant(0.01, shape=[n_output]))
	# input layer
	x = tf.placeholder('float', [None, boardSize])
	# hidden layers
	hidden_layer_1 = tf.nn.relu(tf.add(tf.matmul(x, weights_1), biases_1))
	hidden_layer_2 = tf.nn.relu(tf.add(tf.matmul(hidden_layer_1, weights_2), biases_2))
	hidden_layer_3 = tf.nn.relu(tf.add(tf.matmul(hidden_layer_2, weights_3), biases_3))
	# output layer
	y = tf.add(tf.matmul(hidden_layer_3, weights_output), biases_output)
	# prediction = index of largest Q value in output layer
	prediction = tf.argmax(y[0])
	return x, y, prediction

def train_network():
	print()
	# create neural network
	input_state , Q_outputs, prediction = create_network()
	# initialize target Q placeholder
	target_Q_outputs = tf.placeholder('float', [None, n_output])
	# gets mean difference between target Q values and predicted Q values
	loss =  tf.reduce_mean(tf.square(tf.subtract(target_Q_outputs, Q_outputs)))
	# train the model to minimize the loss
	optimizer = tf.train.AdamOptimizer().minimize(loss)
	# create tensorflow session
	session = tf.InteractiveSession()
	# intialize variables
	session.run(tf.global_variables_initializer())
	# iteration tracker variable
	iterations = 0
	# print epsilon decay rate
	print('Epsilon decay rate is {}'.format(epsilon_decay_rate))
	# main training loop
	while True:
		# reference to global variables
		global won_games
		global lost_games
		global drawn_games
		global epsilon
		# total loss
		total_loss = 0
		# object for holding training data
		game_list = []
		# iterate through a given number of games, storing resulting data in game_list
		for i in range(episodes):
			game,_ = generate_game(epsilon, session, input_state, prediction, Q_outputs)
			# append data to list
			game_list.append(game)
		# of these 500 games, use # of epochs for training set
		for k in range(epochs):
			# shuffle game_list to increase randomness
			random.shuffle(game_list)
			# iterate through game_list
			for current_game in game_list:
				# initialize loop iterator
				iterator = 0
				game_reward = 0
				# iterate through current complete game
				while iterator < len(current_game):
					# pop most recent game_state in current game
					current_game_data = current_game.pop()
					# get current_game_state -- 1st item in current_game_data
					current_game_state = current_game_data[0]
					# action taken -- 2nd item in current_game_data
					action = current_game_data[1][0]
					# reward received -- 3rd item in current_game_data
					reward = current_game_data[2][0]
					# next_state -- 4th item in current_game_data
					next_state = current_game_data[3]
					# if this is first iteration, set game_reward = the received reward
					if iterator == 0:
						game_reward = reward
					else:
						# get Q value predicted by NN for subsequent next_state
						next_Q = session.run(Q_outputs, feed_dict={input_state:[next_state]})
						# get maximum Q value from resulting matrix
						max_next_Q = np.max(next_Q)
						# multiply maximum next Q value by discount factor GAMMA
						game_reward = GAMMA * max_next_Q
					# set target_Q to Q value predicted by NN from current_game_state
					target_Q = session.run(Q_outputs, feed_dict={input_state:[current_game_state]})
					# set reward to -1 for all illegal moves to help network converge more quickly
					for index, item in enumerate(current_game_state):
						if item != 0:
							target_Q[0, index] = -1
					# set target_Q at index of predicted action = reward receieved
					target_Q[0, action] = game_reward
					# increment iterator
					iterator += 1
					# initialize loss for current iteration
					current_loss = 0
					# get current_loss from NN
					current_loss = session.run([optimizer, Q_outputs, loss], feed_dict={input_state:[current_game_state], target_Q_outputs:target_Q})
					# get total loss for all iterations in designated # of episodes
					total_loss += current_loss[2]
		# increment iterations
		iterations += 1
		# print data for each iteration
		print('-'*80)
		print('Iteration {} completed:\nWins: {} Losses: {} Draws: {}\nLoss Percentage: {}\nGames played: {}\nepsilon: {} \ncost: {}'.format
		(iterations, won_games, lost_games, drawn_games, (lost_games/episodes)*100, episodes, epsilon*100, total_loss))
		print('-'*80)
		# reset iteration data variables
		total_loss = 0
		won_games = 0
		lost_games = 0
		drawn_games = 0
		# if epsilon is greater than designated value, reset it randomly
		if epsilon > -0.2:
			# decrease epislon by epsilon_decay_rate
			epsilon -= epsilon_decay_rate
		else:
			# set epsilon to a random value
			epsilon = random.choice([0.1,0.05,0.06,0.07,0.15,0.03,0.20,0.25,0.5,0.4])

# generate a list of game states, actions, and final reward
def generate_game(e, session, input_state, prediction, Q_outputs):
	global won_games
	global lost_games
	global drawn_games

	win_reward = 10
	loss_reward = -1
	draw_reward = 3

	## create the entire game memory object that contains the memories for the game
	## and an empty board
	complete_game_memory = []
	game_board = np.zeros(9).reshape(3,3)

	## randomly chose a turn 1 is ours -1 is oppnents
	turn = random.choice([1,-1])

	## if opponents turn let him play and set the inital state
	if(turn == -1):
		initial_index = random.choice(range(9))
		best_index, _= session.run([prediction,Q_outputs], feed_dict={input_state : [np.array(np.copy(game_board).reshape(-1))]})
		initial_index = random.choice([best_index,initial_index,best_index])
		game_board[int(initial_index/3),initial_index%3] = -1
		turn = turn * -1

	## while the game is not over repat, our move then opponents move
	while(True):

		## create a memory which will hold the current inital state, the action thats taken, the reward the was recieved, the next state
		memory = []

		## create a copy of the board which is linear
		temp_copy = np.array(np.copy(game_board).reshape(-1))

		## fetch all the indexes that are free or zero so those can used for playing next move
		zero_indexes = []
		for index,item in enumerate(temp_copy):
			if item == 0:
				zero_indexes.append(index)

		## if no index is found which is free to place a move exit as the game completed with slight reward. better to draw then to lose right ?
		if len(zero_indexes) == 0:
			reward = draw_reward
			complete_game_memory[-1][2][0] = reward
			drawn_games += 1
			break

		## if free indexs are found randomly select one which will be later can be used as the action.
		selectedRandomIndex = random.choice(zero_indexes)

		## calculate the prediction from the network which can be later used as an action with some probability
		pred, _ = session.run([prediction,Q_outputs], feed_dict={input_state : [temp_copy]})

		## since the netowrk can be messy and inacurate check if the prediction is correct first.
		isFalsePrediction = False if temp_copy[pred] == 0 else True

		## lets add the inital state to the current memory
		memory.append(np.copy(game_board).reshape(-1))

		## Lets pick an action with some probability, exploration and exploitation
		if random.random() > e: #and isFalsePrediction == False: #expliotation
			action = pred
		else: # exploration, explore with valid moves to save time.
			random_action = random.choice(range(9))
			action = selectedRandomIndex
			#action = random.choice([selectedRandomIndex,random_action])
			#action = random.choice(range(9))

		## lets add the action to the memory
		memory.append([action])

		## randomly plays a wrong move.. unlucky, however.
		if action not in zero_indexes:
			reward = loss_reward
			memory.append([reward])
			memory.append(np.copy(game_board.reshape(-1)))
			complete_game_memory.append(memory)
			lost_games +=1
			break

		## update the board with the action taken
		game_board[int(action/3),action%3] = 1
		# print(game_board)

		## now calcualte the reward.
		reward = 0

		## if we choose an action thats invalid, boo we get no reward and opponent wins
		if isFalsePrediction == True and action == pred:
			reward = loss_reward
			memory.append([reward])
			memory.append(np.copy(game_board.reshape(-1)))
			complete_game_memory.append(memory)
			lost_games +=1
			break

		## if after playing our move the game is completed then yay we deserve a reward and its the final state
		if(did_ai_win(game_board)):
			reward = win_reward
			memory.append([reward])
			memory.append(np.copy(game_board.reshape(-1)))
			complete_game_memory.append(memory)
			won_games +=1
			break



		# Now lets make a move for the opponent

		## same as before, but since we are finding a move for the opponent we use the inverse board
		## to calculate the prediction
		temp_copy_inverse = np.array(np.copy(invert_board(game_board)).reshape(-1))
		temp_copy = np.array(np.copy(game_board).reshape(-1))
		zero_indexes = []
		for index,item in enumerate(temp_copy):
			if item == 0:
				zero_indexes.append(index)

		## if opponent has no moves left that means that the last move was the final move and its a draw so some reward
		if len(zero_indexes) == 0:
			reward = draw_reward
			memory.append([reward])
			memory.append(np.copy(game_board.reshape(-1)))
			complete_game_memory.append(memory)
			drawn_games+=1
			break

		## almost same as before
		selectedRandomIndex = random.choice(zero_indexes)
		pred, _ = session.run([prediction,Q_outputs], feed_dict={input_state : [temp_copy_inverse]})
		isFalsePrediction = False if temp_copy[pred] == 0 else True

		## we want opponet to play good sometimes and play bad sometimes so 33.33% ish probability
		action = None

		if(isFalsePrediction == True):
			action = random.choice([selectedRandomIndex])
		else:
			action = random.choice([selectedRandomIndex,pred,pred,pred,pred])

		temp_copy2 = np.copy(game_board).reshape(-1)
		if temp_copy2[action] != 0:
			print('big time error here ',temp_copy2 , action)
			return

		## update the board with opponents move
		game_board[int(action/3),action%3] = -1
		# print(game_board)

		## if after opponents move the game is done meaning opponent won, boo..
		if did_opponent_win(game_board) == True:
			reward = loss_reward
			memory.append([reward])
			#final state
			memory.append(np.copy(game_board.reshape(-1)))
			complete_game_memory.append(memory)
			lost_games +=1
			break

		## if no one won and game isn't done yet then lets continue the game
		memory.append([0])
		memory.append(np.copy(game_board.reshape(-1)))

		## lets add this move to the complete game memory
		complete_game_memory.append(memory)

	# return the complete game memory and the last set reward
	return complete_game_memory,reward


if __name__ == '__main__':
	train_network()